# encoding: utf-8
from enum import Enum
from concurrent.futures import ThreadPoolExecutor

ping_pool = ThreadPoolExecutor()


def detect_net(
    target: str = "1.1.1.1", timeout: int = 1, try_time: int = 5, threshold: int = 2
) -> bool:
    """测试网络连通性

    Args:
        target(str): ping的目标网址
        timeout(int): 等待连接时间
        try_time(int): 尝试次数
        threshold(int): 判定为"连上网了"状态所需要达到连接成功的次数，最大需要小于try_time
    Returns:
        bool: 可以的话，就返回True，否则返回False
    """
    from pythonping import ping
    import asyncio as aio
    from functools import partial

    target_count = min(threshold, try_time)
    connect_count = 0

    async def _ping_signal(target: str, timeout: int):
        nonlocal connect_count
        try:
            ret = await aio.get_event_loop().run_in_executor(
                ping_pool, partial(ping, target=target, timeout=timeout, count=1)
            )
            if ret.success():
                connect_count += 1
        except:
            # 如果ping出了问题，就默认它没有ping通
            pass

    async def _run():
        await aio.gather(
            *[aio.create_task(_ping_signal(target, timeout)) for _ in range(try_time)]
        )
        return connect_count >= target_count

    return aio.run(_run())


class LoggerName(Enum):
    console = "console_out"
    file_out = "file_logger"

    def __eq__(self, __o: object) -> bool:
        if isinstance(__o, LoggerName):
            return self is __o
        elif isinstance(__o, str):
            return self.name.lower() == __o.lower()
        else:
            return False


class ResultTitle(Enum):
    success = "认证成功页"
    logined = "信息页"


class LoginResult(Enum):
    success = "success"
    fail = "fail"

    def __eq__(self, __o: object) -> bool:
        if isinstance(__o, LoginResult):
            return self is __o
        elif isinstance(__o, str):
            return __o.lower() == self.value.lower()
        else:
            return False


def gen_logger(logging_dir: str, logger_name: LoggerName):
    from pathlib import Path
    from logging import config
    import logging

    p = Path(logging_dir)
    if p.is_dir():
        if not p.exists():
            p.mkdir()
        p = p / "schedule.log"
    elif not p.is_file():
        raise FileNotFoundError(f"日志文件路径{p}错误")
    elif p.suffix != ".log":
        return gen_logger(str(p.parent), logger_name)

    # 日志配置
    LOGGING = {
        "version": 1,
        "disable_existing_loggers": False,
        # 格式化器
        "formatters": {
            "standard": {
                "format": "[%(levelname)s] [%(asctime)s] [%(filename)s] [%(funcName)s] [%(lineno)d] > %(message)s"
            },
            "simple": {"format": "[%(levelname)s] > %(message)s"},
        },
        # 处理器
        "handlers": {
            "console": {
                "level": "DEBUG",
                "class": "logging.StreamHandler",
                "formatter": "simple",
            },
            "file_handler": {
                "level": "INFO",
                "class": "logging.FileHandler",
                "filename": str(p.resolve()),  # 具体日志文件的名字
                "formatter": "standard",
                "encoding": "utf-8",
            },  # 用于文件输出
        },
        # 日志记录器
        "loggers": {  # 日志分配到哪个handlers中
            LoggerName.console.value: {  # 后面导入时logging.getLogger使用的app_name
                "handlers": ["console"],
                "level": "DEBUG",
                "propagate": True,
            },
            LoggerName.file_out.value: {
                "handlers": ["file_handler"],
                "level": "INFO",
                "propagate": False,
            },
        },
    }
    config.dictConfig(LOGGING)
    return logging.getLogger(logger_name.value)


if __name__ == "__main__":
    print(detect_net("1.1.1.1", try_time=100))
