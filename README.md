# 自动登录中原工学院校园网
通过requests构造请求体自动化登录校园网的小脚本。还支持自动连接校园网wifi。

## 使用方法
### 1. 直接源码运行
#### 配置开发环境
- 安装python环境（推荐使用`python -m venv`创建虚拟环境，防止污染python公共环境）。
- 使用pip安装以下依赖：
    ```
    pip install -r requirements.txt
    ```

#### 单次连接校园网
1. 直接下载源码解压后进入文件夹，修改account.toml，输入用户名（学号）、密码、运营商，并将schedule设为false。
2. 使用`python campus_net.py`来运行即可。

#### 计划性周期重连校园网
1. 在尝试单次运行校园网成功之后再继续。
2. 将account.toml中的auto_connect_wifi改为true、block改为false以及schedule改为true。
3. 根据配置文件中的提示以及自身的选择来修改account.toml中的detect、every、per和log_path等选项(detect_timeout一般不用修改)。
4. 使用`python campus_net.py`来运行即可。

### 2. 通过已构建的二进制运行
#### 单次连接校园网
1. 通过release界面下载最新版本对应平台的可运行文件。
2. 解压后进入文件夹，修改account.toml，输入学号、密码、运营商，并将schedule设为false。
3. 双击campus_net.exe运行即可。

#### 计划性周期重连校园网
1. 在尝试单次运行校园网成功之后再继续。
2. 将account.toml中的auto_connect_wifi改为true、block改为false、schedule改为true。
3. 根据配置文件中的提示以及自身的选择来修改account.toml中的detect、every、per和log_path等选项(detect_timeout一般不用修改)。
4. 双击campus_net.exe运行即可。

## 其他信息
如果选择了自动连接wifi，可能会由于wifi连接太快导致网络没有反应过来导致弹窗。如果出现此问题，重新运行一次即可。
