from types import MappingProxyType
import rtoml as toml
from pathlib import Path
from tkinter import messagebox as box
import sched
import time

__root_path = Path(__file__).parent

import utils
import campus_net


time_duration = MappingProxyType(
    {"hour": 60 * 60, "day": 60 * 60 * 24, "week": 60 * 60 * 24 * 7}
)


def _schedule(cfg, logger, retry=5):
    if retry <= 0:
        logger.error("连接失败，下个周期会自动重试")
        return

    try:
        if cfg["detect"]:
            if utils.detect_net("baidu.com", cfg["detect_timeout"] / 3, 3, 1):
                logger.info("探测完毕，当前依然存在网络连接，故不再尝试重连")
                return
            else:
                logger.info("网络断开，尝试重连")
    except KeyError:
        cfg["detect"] = False

    try:
        ret = campus_net.run(cfg)
    except Exception as e:
        logger.error(f"校园网连接程序报错:\n{e}")
        logger.warn("登录失败，10秒后重试...")
        time.sleep(10)
        return _schedule(cfg, logger, retry=retry - 1)
    else:
        if ret == utils.LoginResult.success:
            if utils.detect_net():
                return
            else:
                logger.error("无法访问1.1.1.1，下个周期重试")
                return _schedule(cfg, logger, retry=retry-1)
        else:
            logger.warn("登录失败，10秒后重试...")
            time.sleep(10)
            return _schedule(cfg, logger, retry=retry - 1)


def run():
    cfg = toml.load(__root_path / "account.toml")
    try:
        if cfg["block"]:
            box.showerror(
                title="请关闭弹窗选项", message="如果想要周期自动登录网页，请在account中将block设置为false"
            )
            raise KeyError("如果想要周期自动登录网页，请在account中将block设置为false")
        if not cfg["auto_connect_wifi"]:
            raise KeyError("如果想要周期自动登录网页，请在account中将auto_connect_wifi设置为true(linux可能还需要root权限)")
    except KeyError:
        cfg["block"] = False

    logger = utils.gen_logger(cfg.get("log_path", __file__), utils.LoggerName.file_out)

    scheduler = sched.scheduler(time.time, time.sleep)

    logger.info(f"开始周期运行...\n每{cfg.get('every', 1)}{cfg.get('per', 'hour')}运行一次")
    delay = cfg.get("every", 1) * time_duration[cfg.get("per", "hour")]
    try:
        while True:
            scheduler.enter(delay, 0, _schedule, (cfg, logger))
            scheduler.run()
    except KeyboardInterrupt:
        logger.info("停止周期运行...")


if __name__ == "__main__":
    run()
