"""
配置文件配置窗口
本代码由[Tkinter布局助手]生成
当前版本:3.6.1
官网:https://www.pytk.net/tkinter-helper
QQ交流群:788392508
"""
import re
import tkinter.messagebox
from enum import Enum
from pathlib import Path
from tkinter import *
from tkinter.ttk import *
from typing import Dict, Optional
from functools import partial

import rtoml as toml


class WinGUI(Tk):
    widget_dic: Dict[str, Widget] = {}

    def __init__(self):
        super().__init__()
        self.__win()
        self.widget_dic["tk_input_username_input"] = self.__tk_input_username_input(self)
        self.widget_dic["tk_input_password_input"] = self.__tk_input_password_input(self)
        self.widget_dic["tk_label_username_txt"] = self.__tk_label_username_txt(self)
        self.widget_dic["tk_label_password_txt"] = self.__tk_label_password_txt(self)
        self.widget_dic["tk_label_isp_txt"] = self.__tk_label_isp_txt(self)
        self.widget_dic["tk_select_box_isp_selector"] = self.__tk_select_box_isp_selector(self)
        self.widget_dic["tk_button_save"] = self.__tk_button_save(self)
        self.widget_dic["tk_button_schedule"] = self.__tk_button_schedule(self)
        self.widget_dic["tk_check_button_block"] = self.__tk_check_button_block(self)

    def __win(self):
        self.title("校园网连接助手")
        # 设置窗口大小、居中
        width = 302
        height = 362
        screenwidth = self.winfo_screenwidth()
        screenheight = self.winfo_screenheight()
        geometry = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.geometry(geometry)
        self.resizable(width=False, height=False)

        # 自动隐藏滚动条

    def scrollbar_autohide(self, bar, widget):
        self.__scrollbar_hide(bar, widget)
        widget.bind("<Enter>", lambda e: self.__scrollbar_show(bar, widget))
        bar.bind("<Enter>", lambda e: self.__scrollbar_show(bar, widget))
        widget.bind("<Leave>", lambda e: self.__scrollbar_hide(bar, widget))
        bar.bind("<Leave>", lambda e: self.__scrollbar_hide(bar, widget))

    def __scrollbar_show(self, bar, widget):
        bar.lift(widget)

    def __scrollbar_hide(self, bar, widget):
        bar.lower(widget)

    def __tk_input_username_input(self, parent):
        ipt = Entry(parent, )
        ipt.place(x=110, y=40, width=150, height=30)
        return ipt

    def __tk_input_password_input(self, parent):
        ipt = Entry(parent, )
        ipt.place(x=110, y=120, width=150, height=30)
        return ipt

    def __tk_label_username_txt(self, parent):
        label = Label(parent, text="用户名", anchor="center", )
        label.place(x=30, y=40, width=50, height=30)
        return label

    def __tk_label_password_txt(self, parent):
        label = Label(parent, text="密码", anchor="center", )
        label.place(x=30, y=120, width=50, height=30)
        return label

    def __tk_label_isp_txt(self, parent):
        label = Label(parent, text="运营商", anchor="center", )
        label.place(x=30, y=200, width=50, height=30)
        return label

    def __tk_select_box_isp_selector(self, parent):
        cb = Combobox(parent, state="readonly", )
        cb['values'] = ("中国移动", "中国联通", "中国电信", "校园单宽", "校园内网")
        cb.place(x=110, y=200, width=150, height=30)
        return cb

    def __tk_button_save(self, parent):
        btn = Button(parent, text="保存设置", takefocus=False, )
        btn.place(x=20, y=310, width=117, height=39)
        return btn

    def __tk_button_schedule(self, parent):
        btn = Button(parent, text="配置定时运行", takefocus=False, )
        btn.place(x=160, y=310, width=119, height=39)
        return btn

    def __tk_check_button_block(self, parent):
        cb = Checkbutton(parent, text="成功连接后是否弹窗")
        cb.place(x=60, y=260, width=174, height=30)
        return cb


class ErrorField(Enum):
    ALL_RIGHT = 0
    USERNAME = 1
    PASSWORD = 2
    ISP = 3


class Win(WinGUI):
    def __init__(self):
        super().__init__()
        self.__event_bind()

        # 读取现有配置
        self.__cfg_path = Path(__file__).parents[1] / "account.toml"
        self.__cfg = toml.load(self.__cfg_path)
        self.__init_values()

    def __init_values(self) -> None:
        """
        将各个控件中的值进行绑定
        """
        self.__block = IntVar(value=int(self.__cfg["block"]))  # 判断是否选中了弹窗
        self.__username = StringVar(value=self.__cfg["username"])
        self.__password = StringVar(value=self.__cfg["password"])
        self.__isp = StringVar(value=self.__cfg["isp"])

        self.widget_dic["tk_check_button_block"]["variable"] = self.__block
        self.widget_dic["tk_input_username_input"]["textvariable"] = self.__username
        self.widget_dic["tk_input_password_input"]["textvariable"] = self.__password
        self.widget_dic["tk_select_box_isp_selector"]["textvariable"] = self.__isp

    def __inner_save_cfg(self) -> None:
        """
        将收集到的配置填入配置文件中
        """
        [self.__cfg.update(i)
         for i in (
                 {"username": self.__username.get()},
                 {"password": self.__password.get()},
                 {"isp": self.__isp.get()},
                 {"block": bool(self.__block.get())}
         )]
        toml.dump(self.__cfg, self.__cfg_path)

    def save_config(self, evt) -> None:
        """
        检查输入的数据以及将文件保存到toml文件中
        """
        err_hinter = partial(tkinter.messagebox.showerror, "字段错误")
        match self.__check_value():
            case ErrorField.USERNAME:
                err_hinter("用户名错误")
            case ErrorField.PASSWORD:
                err_hinter("密码错误")
            case ErrorField.ISP:
                err_hinter("未知的运营商")
            case ErrorField.ALL_RIGHT:
                self.__inner_save_cfg()
                self.destroy()
                tkinter.messagebox.showinfo("保存成功", "请再次运行此应用")
            case _:
                raise ValueError()

    def __check_value(self) -> ErrorField:
        """
        检查输入的值是否合法
        Returns: 不合法的枚举值，只会返回第一个匹配的错误问题
        """
        m = re.match
        if not m(r'^\d+$', self.__username.get()):
            return ErrorField.USERNAME
        elif not m(r'^\w+$', self.__password.get()):
            return ErrorField.PASSWORD
        elif not m(r"(中国移动|中国联通|中国电信|校园单宽|校园内网)", self.__isp.get()):
            return ErrorField.ISP
        else:
            return ErrorField.ALL_RIGHT

    def config_schedule(self, evt) -> None:
        """
        开启配置定时任务的窗口
        """
        err_hinter = partial(tkinter.messagebox.showerror, "字段错误")
        match self.__check_value():
            case ErrorField.USERNAME:
                err_hinter("用户名错误")
            case ErrorField.PASSWORD:
                err_hinter("密码错误")
            case ErrorField.ISP:
                err_hinter("未知的运营商")
            case ErrorField.ALL_RIGHT:
                import schedule_windows

                if self.__block.get():
                    tkinter.messagebox.showerror("无法开启定时任务", "取消弹窗选项后才能开启定时任务")
                    return

                self.__inner_save_cfg()
                self.withdraw()
                self.destroy()
                win = schedule_windows.Win(self.__cfg_path, self.__cfg)
                win.mainloop()
            case _:
                raise ValueError()

    def __event_bind(self) -> None:
        self.widget_dic["tk_button_save"].bind('<Button>', self.save_config)
        self.widget_dic["tk_button_schedule"].bind('<Button>', self.config_schedule)


if __name__ == "__main__":
    win = Win()
    win.mainloop()
