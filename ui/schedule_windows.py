"""
本代码由[Tkinter布局助手]生成
当前版本:3.6.1
官网:https://www.pytk.net/tkinter-helper
QQ交流群:788392508
"""
import tkinter
from enum import Enum
from functools import partial
from pathlib import Path
from tkinter import *
from tkinter import filedialog
from tkinter.ttk import *
from typing import Dict, Optional
from types import MappingProxyType


class WinGUI(Tk):
    widget_dic: Dict[str, Widget] = {}

    def __init__(self):
        super().__init__()
        self.__win()
        self.widget_dic["tk_check_button_check"] = self.__tk_check_button_check(self)
        self.widget_dic["tk_label_every"] = self.__tk_label_every(self)
        self.widget_dic["tk_input_try_times"] = self.__tk_input_try_times(self)
        self.widget_dic["tk_select_box_per"] = self.__tk_select_box_per(self)
        self.widget_dic["tk_label_retry"] = self.__tk_label_retry(self)
        self.widget_dic["tk_label_log_path_txt"] = self.__tk_label_log_path_txt(self)
        self.widget_dic["tk_input_log_path"] = self.__tk_input_log_path(self)
        self.widget_dic["tk_label_timeout_txt"] = self.__tk_label_timeout_txt(self)
        self.widget_dic["tk_input_timeout_input"] = self.__tk_input_timeout_input(self)
        self.widget_dic["tk_label_second"] = self.__tk_label_second(self)
        self.widget_dic["tk_button_save"] = self.__tk_button_save(self)

    def __win(self):
        self.title("定时运行窗口")
        # 设置窗口大小、居中
        width = 691
        height = 383
        screenwidth = self.winfo_screenwidth()
        screenheight = self.winfo_screenheight()
        geometry = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.geometry(geometry)
        self.resizable(width=False, height=False)

        # 自动隐藏滚动条

    def scrollbar_autohide(self, bar, widget):
        self.__scrollbar_hide(bar, widget)
        widget.bind("<Enter>", lambda e: self.__scrollbar_show(bar, widget))
        bar.bind("<Enter>", lambda e: self.__scrollbar_show(bar, widget))
        widget.bind("<Leave>", lambda e: self.__scrollbar_hide(bar, widget))
        bar.bind("<Leave>", lambda e: self.__scrollbar_hide(bar, widget))

    def __scrollbar_show(self, bar, widget):
        bar.lift(widget)

    def __scrollbar_hide(self, bar, widget):
        bar.lower(widget)

    def __tk_check_button_check(self, parent):
        cb = Checkbutton(parent, text="重连前检测校园网是否依然可用", )
        cb.place(x=200, y=30, width=299, height=67)
        cb.invoke()
        return cb

    def __tk_label_every(self, parent):
        label = Label(parent, text="每", anchor="center", )
        label.place(x=120, y=140, width=59, height=40)
        return label

    def __tk_input_try_times(self, parent):
        ipt = Entry(parent, )
        ipt.place(x=190, y=140, width=158, height=40)
        return ipt

    def __tk_select_box_per(self, parent):
        cb = Combobox(parent, state="readonly", )
        cb['values'] = ("小时", "天", "周")
        cb.place(x=360, y=140, width=80, height=41)
        return cb

    def __tk_label_retry(self, parent):
        label = Label(parent, text="重连一次", anchor="center", )
        label.place(x=450, y=140, width=90, height=38)
        return label

    def __tk_label_log_path_txt(self, parent):
        label = Label(parent, text="运行日志保存地址", anchor="center", )
        label.place(x=100, y=210, width=118, height=39)
        return label

    def __tk_input_log_path(self, parent):
        ipt = Entry(parent, )
        ipt.place(x=240, y=210, width=322, height=39)
        return ipt

    def __tk_label_timeout_txt(self, parent):
        label = Label(parent, text="超时时长", anchor="center", )
        label.place(x=440, y=270, width=50, height=30)
        return label

    def __tk_input_timeout_input(self, parent):
        ipt = Entry(parent, )
        ipt.place(x=500, y=270, width=50, height=30)
        return ipt

    def __tk_label_second(self, parent):
        label = Label(parent, text="秒", anchor="center", )
        label.place(x=560, y=270, width=20, height=30)
        return label

    def __tk_button_save(self, parent):
        btn = Button(parent, text="保存设置", takefocus=False, )
        btn.place(x=90, y=330, width=472, height=30)
        return btn


class ErrorFiled(Enum):
    ALL_RIGHT = 0
    DETECT = 1
    TRY_TIMES = 2
    PER = 3
    LOG_PATH = 4
    TIMEOUT = 5


class Win(WinGUI):
    def __init__(self, config_path: Path = Path(__file__).parents[1] / "account.toml"
            , config_dir: Optional[dict] = None):
        super().__init__()
        self.__time_unit = MappingProxyType({
                "小时": "hour",
                "天": "day",
                "周": "week"
        })
        self.__event_bind()
        self.__cfg_path = config_path
        if not config_dir:
            from rtoml import load
            self.__cfg = load(config_path)
        else:
            self.__cfg = config_dir

        self.__init_values()

    def __init_values(self):
        self.__detect = IntVar(value=int(self.__cfg["detect"]))
        self.__every = DoubleVar(value=float(self.__cfg["every"]))
        self.__per = StringVar(value="")
        self.__log_path = StringVar(value=self.__cfg["log_path"])
        self.__timeout = IntVar(value=self.__cfg["detect_timeout"])

        self.widget_dic["tk_check_button_check"]["variable"] = self.__detect
        self.widget_dic["tk_input_try_times"]["textvariable"] = self.__every
        self.widget_dic["tk_select_box_per"]["textvariable"] = self.__per
        self.widget_dic["tk_input_log_path"]["textvariable"] = self.__log_path
        self.widget_dic["tk_input_timeout_input"]["textvariable"] = self.__timeout

    def select_path(self, evt):
        path = filedialog.asksaveasfilename(defaultextension=".log",
                                            filetypes=[("Log Files", "*.log")])
        self.__log_path.set(path)

    def __check(self) -> ErrorFiled:
        """
        检查表单并返回第一个匹配到的错误字段
        Returns: 匹配到的第一个错误字段
        """
        log_path = Path(self.__log_path.get())
        try:
            self.__every.get()
        except TclError:
            return ErrorFiled.TRY_TIMES

        try:
            self.__timeout.get()
        except TclError:
            return ErrorFiled.TIMEOUT

        if self.__every.get() <= 1e-2:
            return ErrorFiled.TRY_TIMES
        elif self.__per not in {"hour", "day", "week"}:
            return ErrorFiled.PER
        elif self.__log_path.get() != "" and (not log_path.exists() or not log_path.is_file()):
            return ErrorFiled.LOG_PATH
        elif self.__timeout.get() <= 1:
            return ErrorFiled.TIMEOUT
        else:
            return ErrorFiled.ALL_RIGHT

    def save_config(self, evt):
        err_hinter = partial(tkinter.messagebox.showerror, "字段错误")
        self.__cfg["schedule"] = True
        print(self.__every.get())
        match self.__check():
            case ErrorFiled.TIMEOUT:
                err_hinter("超时时间请设置为大于1的数字")
            case ErrorFiled.PER:
                err_hinter("重试时间单位错误")
            case ErrorFiled.TRY_TIMES:
                err_hinter("重试间隔时间请为正整数")
            case ErrorFiled.LOG_PATH:
                err_hinter("不合法的日志路径")
            case ErrorFiled.DETECT:
                err_hinter("发生了什么事? 这个不可能会出现问题的呀")
            case ErrorFiled.ALL_RIGHT:
                from rtoml import dump
                [self.__cfg.update(i)
                 for i in (
                         {"detect": bool(self.__detect.get())},
                         {"every": self.__every.get()},
                         {"per": self.__time_unit[self.__per.get()]},
                         {"log_path": self.__log_path.get()},
                         {"detect_timeout": self.__timeout.get()}
                 )]
                dump(self.__cfg, self.__cfg_path)
                self.destroy()
                tkinter.messagebox.showinfo("保存成功", "请再次运行此应用")
            case _:
                raise ValueError()

    def __event_bind(self):
        self.widget_dic["tk_input_log_path"].bind('<Button>', self.select_path)
        self.widget_dic["tk_button_save"].bind('<Button>', self.save_config)


if __name__ == "__main__":
    win = Win()
    win.mainloop()
